package com.kfwebstandard.beans;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/**
 * Backing bean with Ajax invoked validator that will throw an exception if you
 * enter an underscore
 *
 * @author Ken Fogel
 */
@Named("user")
@RequestScoped
public class UserBean implements Serializable {

    private String name = "";
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String newValue) {
        name = newValue;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String newValue) {
        password = newValue;
    }

    /**
     * Method called to validate the input. The validation rule is that there
     * cannot be an underscore in the string
     *
     * @param fc
     * @param c
     * @param value
     */
    public void validateName(FacesContext fc, UIComponent c, Object value) {
        if (((String) value).contains("_")) {
            throw new ValidatorException(new FacesMessage(
                    "Name cannot contain underscores"));
        }
    }
}
